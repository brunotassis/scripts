﻿class ScriptOption {
    <# VARIAVEIS #>
    [string]$DisplayTitle
    [string]$Slug
    [string]$Path

    <# CONSTRUTOR #>
    ScriptOption() { }

    ScriptOption(
        [string]$DisplayTitle,
        [string]$Slug,
        [string]$Path
    ) {
        $this.DisplayTitle = $DisplayTitle
        $this.Slug = $Slug
        $this.Path = $Path
    }

    <# METODOS #>
    [String]GetDisplayUpper() {
        Return $this.DisplayTitle.ToUpper();
    }

    [Void]RunScript() {
        if (-not [string]::IsNullOrEmpty($this.Path)) {
            powershell.exe $this.Path;
        }
        else {
            Write-Host 'CAMINHO PARA EXECUTAR O SCRIPT NÃO ENCONTRADO!';
        }
    }

    <# == EXEMPLOS ================================================================ #>
    # Novo objeto
    # $Objeto = [ScriptOption]::New();
    # $Objeto = New-Object ScriptOption;

    # Atribuição de parametros
    # $Objeto.DisplayTitle = "Script Title";

    # Novo objeto com parametros
    # $Objeto = [ScriptOption]::New('Script Title', 'script-slug', '\script\path');
    <# == EXEMPLOS ================================================================ #>
}

function Menu() {
    param(
        [System.Collections.ArrayList]$ArrayItens,
        [string]$ReadMessage,
        [bool]$ClearHost = $true
    )

    if ($ClearHost) { Clear-Host; }

    if ($ArrayItens.Count -ge 0) {
        for ($i = 0; $i -lt $ArrayItens.Count; $i++) {
            $ID = $i + 1;
            Write-Host "[${ID}]" $ArrayItens[$i].GetDisplayUpper();
        };

        Write-Host "[0] SAIR";
        Write-Host "";

        $Option = Read-Host $ReadMessage;
        CallScript $ArrayList $Option;
    }
    else {
        Write-Host "NÃO FORAM ENCONTRADOR ITENS NESTE MENU!";
        exit;
    }
}

function CallScript() {
    param(
        $ArrayList,
        $Option
    )

    $ID = $Options - 1;

    switch ($Option) {
        1 {
            $ArrayList[$ID].RunScript();
            Menu $ArrayList 'Tudo Ok! O que mais deseja fazer?' $true;
        }

        0 { exit }

        Default { Menu $ArrayList 'OPÇÃO INVALIDA, POR FAVOR TENTE OUTRO VALOR' $true; }
    }
}

<# == SCRIPT MAIN =================================================================== #>

[System.Collections.ArrayList]$ArrayList = @()
$ArrayList.Add([ScriptOption]::New('CORREÇÃO DE BUGS', 'bug-correct', '\script\path'));
$ArrayList.Add([ScriptOption]::New('DESBLOQUEAR DISCO LOCAL D:', 'unlock-local-disk-d', '\script\path'));
# $ArrayList.Add([ScriptOption]::New('Script Title 3', 'script-slug', '\script\path'));

Menu $ArrayList 'Olá, escolha uma das opções acima' $true;

