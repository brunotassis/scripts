## Comandos Bitlocker (Windows)

#### 1. Destravar uma partição
> \$SecureString = ConvertTo-SecureString "YOUR_PASSWORD" -AsPlainText -Force
> Unlock-BitLocker -MountPoint "D:" -Password $SecureString

#### 2. Travar uma partição
> Lock-BitLocker -MountPoint "E:" -ForceDismount

#### 3. Status do Volume
> 

[:earth_americas: Documentação: (Windows 10 and Windows Server 2016)](https://docs.microsoft.com/en-us/powershell/windows/get-started?view=win10-ps)