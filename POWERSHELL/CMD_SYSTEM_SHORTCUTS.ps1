# == Variaveis ==================================================================================== #
$OPT_MENU_MAIN = New-Object System.Collections.ArrayList
$OPT_MENU_MAIN.Add | MakeOptionObject -displayTitle "" -command "";

# == Funções de Suporte =========================================================================== #
function MakeOptionObject {
    param 
    (
        $command,
        $description,
        $displayTitle
    )
    
    $Object = New-Object System.Object;
    
    $Object = Add-Member -type NoteProperty -name Command -value $command;
    $Object = Add-Member -type NoteProperty -name Description -value $description;
    $Object = Add-Member -type NoteProperty -name DisplayTitle -value $displayTitle;

    return $Object;
}

function MakeOptionTitle{
    param($Title, $BreakLine = "")

    Write-Host ("=" * ($Title.Length * 2));
    Write-Host (" " * ($Title.Length / 2)) $Title (" " * ($Title.Length / 2));
    Write-Host ("=" * ($Title.Length * 2)) $BreakLine;
}

# == Funções ====================================================================================== #

function InitMenu{
    $SelectedOption = 0;

    $Title = "POWERSHELL COMMAND SHORTCUT'S";
    Write-Host ("=" * ($Title.Length * 2));
    Write-Host (" " * ($Title.Length / 2)) $Title (" " * ($Title.Length / 2));
    Write-Host ("=" * ($Title.Length * 2)) `n;

    $BreakLine = ("=" * ($Title.Length * 2));

    Write-Host("1) Habilitar Administrador.");
    Write-Host("2) Desabilitar Administrador.");

    Write-Host($BreakLine);

    Write-Host("3) Criar novo usuario.");
    Write-Host("4) Excluir usuario.");

    Write-Host($BreakLine);

    Write-Host("5) Exibir dispositivos de rede.");

    Write-Host($BreakLine);

    Write-Host("0) Excluir usuario.");

    $SelectedOption = Read-Host "`nSelecione a opção desejada";

    switch ($SelectedOption) {
        1 { 
            Write-Host 'Habilitando usuario Administrador...'
            net user Administrador /active:yes 
        }
        2 { 
            Write-Host 'Desabilitando usuario Administrador...'
            net user Administrador /active:no 
        }
        3 { CreateUser }
        4 { DeleteUser }
        0 { Clear-Host; Write-Host("Finalizando Script..."); exit; }

        Default {
            Clear-Host;
            Write-Host "Por favor digite uma das opções abaixo. `n" -BackgroundColor red -ForegroundColor white;
            InitMenu;
        }
    }
}

function CreateUser {
    param (
        [string]$username,
        [string]$password
    )
    
    # Init Function

}

function DeleteUser {
    Clear-Host;

    $IndexNumber = 0;
    $ComputerUsers = Get-WmiObject -Class Win32_UserAccount;
    
    $Title = "LOCAL USERS";
    Write-Host ("=" * ($Title.Length * 2));
    Write-Host (" " * ($Title.Length / 2)) $Title (" " * ($Title.Length / 2));
    Write-Host ("=" * ($Title.Length * 2)) `n;

    foreach ($User in $ComputerUsers) {
        $IndexNumber++;
        Write-Host $IndexNumber")" $User.Name;
    }

    $SelectedOption = Read-Host "`nPor favor escolha qual usuario deseja excluir";

    Write-Host($ComputerUsers[$SelectedOption]);
}

function ListIPs {
    Get-NetIpAddress | Sort-Object -Property InterfaceAlias | Format-Table
    @{Label="Interface"; Expression={$_.InterfaceAlias}}, 
    @{Label="IP Family"; Expression={$_.AddressFamily}},
    @{Label="Endereço IP"; Expression={$_.IPAddress}}
}

# == Init Script ================================================================================== #
Clear-Host; 
InitMenu;
