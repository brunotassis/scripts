# Script de atalhos do Visual Studio Code.
# Tenha devidamente instalado o programa Visual Studio para ultilização deste script.
# Verifique se o comando "code" esta devidamento configurado nas variaveis de ambiente.

Clear-Host;

function MakeCommandObject {
    param
    (
        $command,
        $description 
    )

    $Object = New-Object System.Object
    
    $Object | Add-Member -type NoteProperty -name Command -value $command
    $Object | Add-Member -type NoteProperty -name Description -value $description

    return $Object 
}

function SendCodeCommand {
    param($command)
    code $command
}

$Options = New-Object System.Collections.ArrayList;
$Options.Add | MakeCommandObject -description "Listar Extensões" -command "--list-extensions";
