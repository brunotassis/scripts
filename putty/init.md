## PUTTY COMMANDS

##### 1. Acesso SSH com IP.
> putty -ssh username@0.0.0.0 -P port -pw password

##### 2. Acesso SSH com Hostname.
> putty -ssh username@hostname -P port -pw password

##### 3. Acesso SSH via sessão.
> putty -load "nome_da_sessão"

[Documentação Oficial](https://the.earth.li/~sgtatham/putty/0.73/htmldoc/)
